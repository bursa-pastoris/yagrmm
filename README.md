**Note:** since 2023-03-15, this script is part of
<https://git.disroot.org/bursa-pastoris/scripts>.  Therefore, this repository
is archived and will no longer be updated. *A bash version* of `yagrmm` is
still maintained as part of the linked repository.

`yagrmm` (Yet Another Git Remote Mirror Manager) is a simple script to
automatically update remote mirrors or git repositories.  At the moment
`yagrmm` only works only if push and pull can be done without SSH keys without
passphrase or without authentication at all.

Whenever `yagrmm` is run, it will pull new commits from the original repo and
push them to the mirror.  It can be run manually or periodically - e.g. with
`crontab`.

## Notice - Please read this

Some software provide the option to configure mirroring from the web interface,
so that the server automatically pulls commits from the original repo - that's
the case for
[GitLab](https://docs.gitlab.com/ee/user/project/repository/mirror/) and
[Gitea](https://docs.gitea.io/en-us/repo-mirror/#pulling-from-a-remote-repository).
If your provider uses one of these softwares but has disabled such function, he
probably has good reasons for doing so - usually limited server resources.  If
your run `yagrmm` periodically, please limit yourself to the strictly necessary
and do not hoard your provider's resources.

## How-to

To use `yagrmm`, follow these steps.

1. Create a local mirror of the repository to mirror with `git clone --mirror
   <original-repo-URL>`.
2. Add the path of the mirror to `~/.config/yagrmm/repolist`.  `~` can be used
   to refer to `/home/<user>` and is always expanded.
3. In the copy of the repository, edit `config` and in the section `[remote
   "origin"]` add the following line below the one beginning with `URL = `:

        pushUrl = <remote-mirror-URL>

    so that the resulting section looks like

        [remote "origin"]
            url = <original-repo-URL>
            pushUrl = <remote-mirror-URL>
            fetch = +refs/*:refs/*
            mirror = true


If - as you should - you push to the mirror with a key different from the one
you use to manual pushes to other repos (such as
[GitLab](https://docs.gitlab.com/ee/user/project/deploy_keys/) and Gitea's
deploy keys), you may find useful a trick to make SSH use the right one.  You
must configure SSH adding a paragraph like the following to `~/.ssh/config`:

    Host mirror.<remote-mirror-domain-name>
        HostName <remote-mirror-domain-name>
        IdentityFile <remote-mirror_push_key>

For example:

    Host mirror.disroot.org
        HostName disroot.org
        IdentityFile ~/.ssh/mirror_push_key

And then you can use `mirror.<remote-mirror-domain-name>` as the domain name
for the `pushUrl`, for example

        pushUrl = git@mirror.disroot.org:username/disroot-howto-mirror.git

# License

`yagrmm` is distributed under the [Expat (aka MIT) license](./LICENSE).
