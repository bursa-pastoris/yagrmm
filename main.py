#!/usr/bin/python3

from pathlib import Path as path
from subprocess import run

home = str(path.home())

with open(home+"/.config/yagrmm/repolist") as repolist_file:
    repolist = repolist_file.read().splitlines()

for i in range(len(repolist)):
    repolist[i] = repolist[i].replace("~", home)

for repo in repolist:
    print(repo)
    run(["git", "-C", repo, "pull"])
    run(["git", "-C", repo, "push"])
